import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import {CardsComponent} from './cards/components/cards-component/cards.component';
import {ExpandableCardsComponent} from './cards/components/expandable-cards/expandable-cards.component';
import {DatepickerComponent} from './datepicker/datepicker.component';
import { WizardComponent } from './wizard/wizard.component';
import {ColorsComponent} from './colors/colors.component';
import {TypographyComponent} from './typography/typography.component';
import {ButtonsComponent} from './buttons/buttons.component';
import {ProfileCardComponent} from './cards/components/profile-card/profile-card.component';
import {AdvancedSearchComponent} from './forms/components/advanced-search/advanced-search.component';
import {DropdownFormComponent} from './forms/components/dropdown-form/dropdown-form.component';
import {ExpandableFormComponent} from './forms/components/expandable-form/expandable-form.component';
import {SimpleFormComponent} from './forms/components/simple-form/simple-form.component';
import {GroupListsComponent} from './lists/components/group-lists/group-lists.component';
import {SimpleListComponent} from './lists/components/simple-list/simple-list.component';
import {CheckboxesComponent} from './checkboxes/checkboxes.component';
import {ModalsDialogComponent} from './modals/components/modals-dialog/modals-dialog.component';
import {ModalsDoughnutComponent} from './modals/components/modals-doughnut/modals-doughnut.component';
import {GraduationRulesComponent} from './previews/components/graduation-rules/graduation-rules.component';
import {GraduationProgressComponent} from './previews/components/graduation-progress/graduation-progress.component';
import { GraduationSimplifiedComponent } from './previews/components/graduation-simplified/graduation-simplified.component';

export const routes: Routes = [
  {
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Home',
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: '/buttons'
      }
    ]
  },
  {
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Components',
      show: true
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'buttons'
      },
      {
        path: 'buttons',
        component: ButtonsComponent,
        data: {
          title: 'Buttons',
          show: true
        }
      },
      {
        path: 'cards',
        data: {
          title: 'Cards',
          show: true
        },
        children: [
          {
            path: 'cards',
            component: CardsComponent,
            data: {
              title: 'Cards',
              show: true
            }
          },
          {
            path: 'expandable-cards',
            component: ExpandableCardsComponent,
            data: {
              title: 'Expandable Cards',
              show: true
            }
          },
          {
            path: 'profile-cards',
            component: ProfileCardComponent,
            data: {
              title: 'Profile Cards',
              show: true
            }
          }
        ]
      },
      {
        path: 'checkboxes',
        component: CheckboxesComponent,
        data: {
          title: 'Checkboxes',
          show: true
        }
      },
      {
        path: 'colors',
        component: ColorsComponent,
        data: {
          title: 'Colors',
          show: true
        }
      },
      {
        path: 'datepicker',
        component: DatepickerComponent,
        data: {
          title: 'Datepicker',
          show: true
        }
      },
      {
        path: 'forms',
        data: {
          title: 'Forms',
          show: true
        },
        children: [
          {
            path: 'advanced-search',
            component: AdvancedSearchComponent,
            data: {
              title: 'Advanced Search',
              show: true
            }
          },
          {
            path: 'dropdown-form',
            component: DropdownFormComponent,
            data: {
              title: 'Dropdown Form',
              show: true
            }
          },
          {
            path: 'expandable-form',
            component: ExpandableFormComponent,
            data: {
              title: 'Expandable Form',
              show: true
            }
          },
          {
            path: 'simple-form',
            component: SimpleFormComponent,
            data: {
              title: 'Simple Form',
              show: true
            }
          }
        ]
      },
      {
        path: 'lists',
        data: {
          title: 'Lists',
          show: true
        },
        children: [
          {
            path: 'group-list',
            component: GroupListsComponent,
            data: {
              title: 'Group List',
              show: true
            }
          },
          {
            path: 'simple-list',
            component: SimpleListComponent,
            data: {
              title: 'Simple List',
              show: true
            }
          }
        ]
      },
      {
        path: 'modals',
        data: {
          title: 'Modals',
          show: true
        },
        children: [
          {
            path: 'modals-dialog',
            component: ModalsDialogComponent,
            data: {
              title: 'Modals Dialog',
              show: true
            }
          },
          {
            path: 'modals-doughnut',
            component: ModalsDoughnutComponent,
            data: {
              title: 'Modals Doughnut',
              show: true
            }
          }
        ]
      },
      {
        path: 'modals',
        data: {
          title: 'Modals',
          show: true
        },
        children: [
          {
            path: 'modal-dialog',
            component: GroupListsComponent,
            data: {
              title: 'Group List',
              show: true
            }
          },
          {
            path: 'simple-list',
            component: SimpleListComponent,
            data: {
              title: 'Simple List',
              show: true
            }
          }
        ]
      },
      {
        path: 'typography',
        component: TypographyComponent,
        data: {
          title: 'Typography',
          show: true
        }
      },
      {
        path: 'wizard',
        component: WizardComponent,
        data: {
          title: 'Wizard',
          show: true
        }
      },
      {
        path: 'formio',
        loadChildren: './formio-examples/formio-examples.module#FormioExamplesModule'
      },
      {
        path: 'router-modals',
        loadChildren: './router-modals/use-router-modals.module#UseRouterModalsModule'
      },
      {
        path: 'previews',
        data: {
          title: 'Previews',
          show: true
        },
        children: [
          {
            path: 'graduation-rules',
            component: GraduationRulesComponent,
            data: {
              title: 'Graduation Rules',
              show: true
            }
          },
          {
            path: 'graduation-progress',
            component: GraduationProgressComponent,
            data: {
              title: 'Graduation Progress',
              show: true
            }
          },
          {
            path: 'graduation-simplified',
            component: GraduationSimplifiedComponent,
            data: {
              title: 'Simplified Graduaton Rules',
              show: true
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    paramsInheritanceStrategy: 'always'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
