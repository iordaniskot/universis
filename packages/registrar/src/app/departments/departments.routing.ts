import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DepartmentsHomeComponent } from './components/departments-home/departments-home.component';
import { DepartmentsTableComponent } from './components/departments-table/departments-table.component';
import { DepartmentsRootComponent } from './components/departments-root/departments-root.component';
import { DepartmentsPreviewComponent } from './components/departments-preview/departments-preview.component';
// tslint:disable-next-line:max-line-length
import {AdvancedFormItemWithLocalesResolver, AdvancedFormRouterComponent} from '../registrar-shared/advanced-form-router/advanced-form-router.component';
import {
  ActiveDepartmentResolver,
  CurrentAcademicPeriodResolver,
  CurrentAcademicYearResolver, LastStudyProgramResolver, ActiveDepartmentIDResolver
} from '../registrar-shared/services/activeDepartmentService.service';
import { AdvancedListComponent } from '@universis/ngx-tables';
// tslint:disable-next-line:max-line-length
import {AdvancedFormModalComponent, AdvancedFormModalData, AdvancedFormItemResolver, AdvancedFormResolver, AdvancedFormParentItemResolver} from '@universis/forms';
import * as DepartmentDocumentSeriesConfig from './documents-series.config.list.json';
import * as DepartmentDocumentSeriesEdit from './forms/DepartmentDocumentNumberSeries/edit.json';
import * as DepartmentDocumentSeriesNew from './forms/DepartmentDocumentNumberSeries/new.json';
import * as InstituteDocumentSeriesConfig from './institute-series.config.list.json';
import * as InstituteDocumentSeriesEdit from './forms/InstituteDocumentNumberSeries/edit.json';
import * as InstituteDocumentSeriesNew from './forms/InstituteDocumentNumberSeries/new.json';
// tslint:disable-next-line:max-line-length
import {DepartmentsTableConfigurationResolver, DepartmentsTableSearchResolver} from './components/departments-table/departments-table-config.resolver';
import {DepartmentsDashboardComponent} from './components/departements-dashboard/departments-dashboard.component';
// tslint:disable-next-line:max-line-length
import {DepartmentsDashboardOverviewComponent} from './components/departements-dashboard/departments-dashboard-overview/departments-dashboard-overview.component';
// tslint:disable-next-line:max-line-length
import {DepartmentsDashboardRegistrationsComponent} from './components/departements-dashboard/departments-dashboard-registrations/departments-dashboard-registrations.component';
import {ArchivedDocumentsHomeComponent} from './components/archived-documents-home/archived-documents-home.component';
import { ArchivedDocumentsTableComponent } from './components/archived-documents-table/archived-documents-table.component';
import { ArchivedDocumentsConfigurationResolver } from './components/archived-documents-table/archived-documents-table-config.resolver';
// tslint:disable-next-line:max-line-length
import { ArchivedDocumentsConfigurationSearchResolver } from './components/archived-documents-table/archived-documents-table-config.resolver';
import * as InstituteTableConfiguration from './institutes.config.list.json';
import * as InstituteSearchConfiguration from './institutes.search.list.json';
import * as DepartmentReportVariableValueConfiguration from './report-variables.config.json';
import {DepartmentSnapshotsComponent} from './components/departments-snapshots/department-snapshots.component';
// tslint:disable-next-line:max-line-length
import { DepartmentsPreviewUsersComponent } from './components/departments-preview/departments-preview-users/departments-preview-users.component';
import { DepartmentsAddUserComponent } from './components/departments-preview/departments-preview-users/departments-add-user.component';
import { DepartmentsSharedModule } from './departments.shared';
import { ActiveDepartmentSnapshotResolver, DepartmentSnapshotVariablesComponent } from './components/departments-snapshots/department-snapshot-variables.component';
import {ValidatePreviewResultsComponent} from './components/validate-preview-results/validate-preview-results.component';
import {
  ValidateResultsDefaultTableConfigurationResolver
} from './components/validate-preview-results/validate-preview-results-config.resolver';
import {GraduationValidationsHomeComponent} from './components/validate-preview-results/graduation-validattions-home/graduation-validations-home.component';

const routes: Routes = [
    {
        path: '',
        component: DepartmentsHomeComponent,
        data: {
            title: 'Departments'
        },
        children: [
            {
            path: '',
            pathMatch: 'full',
            redirectTo: 'list/index'
          },
          {
            path: 'list',
            pathMatch: 'full',
            redirectTo: 'list/undergraduate'
          },
          {
            path: 'list/:list',
            component: DepartmentsTableComponent,
            data: {
              title: 'Departments List'
            },
            resolve: {
              tableConfiguration: DepartmentsTableConfigurationResolver,
              searchConfiguration: DepartmentsTableSearchResolver
            },

          }
        ]
    },
  {
    path: 'create',
    component: DepartmentsRootComponent,
    children: [
      {
        path: 'new',
        pathMatch: 'full',
        component: AdvancedFormRouterComponent,
        data: {
        },
        resolve: {
          department: ActiveDepartmentResolver,
          inscriptionYear: CurrentAcademicYearResolver,
          inscriptionPeriod: CurrentAcademicPeriodResolver,
          studyProgram: LastStudyProgramResolver
        }
      }
    ]
  },
    {
      path: 'new',
      component: AdvancedFormRouterComponent,
      resolve: {
        department: ActiveDepartmentResolver,
        inscriptionYear: CurrentAcademicYearResolver,
        inscriptionPeriod: CurrentAcademicPeriodResolver,
        studyProgram: LastStudyProgramResolver
      }
    },
    {
      path: 'current/documents',
      component: ArchivedDocumentsHomeComponent,
      resolve: {
        department: ActiveDepartmentResolver
      },
      children: [
        {
          path: 'series/:documentSeries/items/:list',
          component: ArchivedDocumentsTableComponent,
          resolve: {
            documents: ArchivedDocumentsConfigurationResolver,
            searchConfiguration: ArchivedDocumentsConfigurationSearchResolver
          }
        }]
    },
  {
    path: 'current/graduation-validations',
    component: GraduationValidationsHomeComponent,
    resolve: {
      department: ActiveDepartmentResolver
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'results'
      },
      {
        path: 'results',
        component: ValidatePreviewResultsComponent,
        resolve: {
           searchConfiguration: ValidateResultsDefaultTableConfigurationResolver
        }

      }
       ]
  },
  {
    path: 'configuration/institutes/series',
    component: AdvancedListComponent,
    data: {
      model: 'InstituteDocumentNumberSeries',
      tableConfiguration: InstituteDocumentSeriesConfig,
      category: 'Sidebar.Departments',
      description: 'Documents.Lists.InstituteDocumentNumberSeries.Description',
      longDescription: 'Documents.Lists.InstituteDocumentNumberSeries.LongDescription'
    },
    children: [
      {
        path: 'add',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'new',
          closeOnSubmit: true,
          formConfig: InstituteDocumentSeriesNew
        }
      },
      {
        path: ':id/edit',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'edit',
          closeOnSubmit: true,
          formConfig: InstituteDocumentSeriesEdit,
        },
        resolve: {
          data: AdvancedFormItemResolver
        }
      }
    ]
  },
  {
    path: 'configuration/institutes',
    component: AdvancedListComponent,
    data: {
      model: 'Institutes',
      tableConfiguration: InstituteTableConfiguration,
      searchConfiguration: InstituteSearchConfiguration,
      category: 'Institutes',
      description: 'Settings.Lists.Institute.Description',
      longDescription: 'Settings.Lists.Institute.LongDescription'
    },
    children: [
      {
        path: 'add',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'new',
          closeOnSubmit: true
        },
        resolve: {
        }
      },
      {
        path: ':id/edit',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'edit',
          serviceQueryParams: {
            $expand: 'registrationPeriods,locales,instituteConfiguration($expand=usernameFormatSource)'
          },
          closeOnSubmit: true,
        },
        resolve: {
          data: AdvancedFormItemWithLocalesResolver
        }
      }
    ]
  },
  {
    path: 'current/reportVariables',
    component: AdvancedListComponent,
    data: {
      model: 'DepartmentReportVariableValues',
      tableConfiguration: DepartmentReportVariableValueConfiguration,
      category: 'Departments',
      description: 'Settings.Lists.DepartmentReportVariableValue.Description',
      longDescription: 'Settings.Lists.DepartmentReportVariableValue.LongDescription'
    },
    children: [
      {
        path: 'add',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'new',
          closeOnSubmit: true,
          description: null
        },
        resolve: {
          department: ActiveDepartmentIDResolver
        }
      },
      {
        path: ':id/edit',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'edit',
          serviceQueryParams: {
          },
          closeOnSubmit: true,
        },
        resolve: {
          data: AdvancedFormItemResolver
        }
      }
    ]
  },
    {
        path: 'current/documents/series',
        component: AdvancedListComponent,
        data: {
            model: 'DepartmentDocumentNumberSeries',
            tableConfiguration: DepartmentDocumentSeriesConfig,
            category: 'Sidebar.Departments',
            description: 'Documents.Lists.DepartmentDocumentNumberSeries.Description',
            longDescription: 'Documents.Lists.DepartmentDocumentNumberSeries.LongDescription'
        },
        children: [
            {
                path: 'add',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                    action: 'new',
                    closeOnSubmit: true,
                    formConfig: DepartmentDocumentSeriesNew
                },
                resolve: {
                    department: ActiveDepartmentIDResolver
                }
            },
            {
                path: ':id/edit',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                    action: 'edit',
                    closeOnSubmit: true,
                    formConfig: DepartmentDocumentSeriesEdit,
                    serviceQueryParams: {
                      $expand: 'parent'
                   },
                },
                resolve: {
                    data: AdvancedFormItemResolver,
                    department: ActiveDepartmentIDResolver
                }
            }
        ]
    },
    {
        path: ':id',
        component: DepartmentsRootComponent,
        data: {
            title: 'Departments Home'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'dashboard'
            },
            {
                path: 'dashboard',
                component: DepartmentsDashboardComponent,
                data: {
                    title: 'Departments Dashboard'
                },
                children: [
                    {
                        path: '',
                        redirectTo: 'general'
                    },
                    {
                        path: 'general',
                        component: DepartmentsDashboardOverviewComponent,
                        data: {
                            title: 'Department\'s View'
                        }
                    },
                    {
                      path: 'history/:snapshot/variables',
                      component: DepartmentSnapshotVariablesComponent,
                      data: {
                        model: 'DepartmentReportVariableSnapshots',
                        description: 'Settings.Lists.DepartmentReportVariableValue.Description',
                        longDescription: 'Settings.Lists.DepartmentReportVariableValue.LongDescription'
                      },
                      children: [
                        {
                          path: 'add',
                          pathMatch: 'full',
                          component: AdvancedFormModalComponent,
                          outlet: 'modal',
                          data: <AdvancedFormModalData>{
                            action: 'new',
                            closeOnSubmit: true,
                            description: null
                          },
                          resolve: {
                            department: ActiveDepartmentSnapshotResolver
                          }
                        },
                        {
                          path: ':id/edit',
                          pathMatch: 'full',
                          component: AdvancedFormModalComponent,
                          outlet: 'modal',
                          data: <AdvancedFormModalData>{
                            action: 'edit',
                            serviceQueryParams: {
                            },
                            closeOnSubmit: true,
                          },
                          resolve: {
                            data: AdvancedFormItemResolver
                          }
                        }
                      ]
                    },
                  {
                    path: 'history',
                    component: DepartmentSnapshotsComponent,
                    children: [
                      {
                        path: ':id/edit',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                          model: 'DepartmentSnapshots',
                          action: 'edit',
                          serviceQueryParams: {
                            $expand: 'locales, reportVariables'
                          },
                          closeOnSubmit: true
                        },
                        resolve: {
                          formConfig: AdvancedFormResolver,
                          data: AdvancedFormItemWithLocalesResolver
                        }
                      },
                    ]
                  },
                  {
                    path: 'users',
                    component: DepartmentsPreviewUsersComponent,
                    children: [
                      {
                        path: 'add',
                        pathMatch: 'full',
                        component: DepartmentsAddUserComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData> {
                          title: 'Departments.AddUser',
                          config: DepartmentsSharedModule.UsersList
                        },
                        resolve: {
                          department: AdvancedFormParentItemResolver
                        }
                      }
                    ]
                  },
                  {
                    path: 'registration',
                    component: DepartmentsDashboardRegistrationsComponent,
                    data: {
                      title: 'Theses Registrations Rules'
                    }
                  }
                ]

            },
            {
              path: 'edit',
              component: AdvancedFormRouterComponent,
              data: {
                action: 'edit',
                serviceQueryParams: {
                  $expand: 'locales'
                }
              },
              resolve: {
                data: AdvancedFormItemWithLocalesResolver
              }
            },
            {
                path: ':action',
                component: AdvancedFormRouterComponent
            }
        ]
    },
    {
      path: 'configuration/degree-templates',
      loadChildren: '../degree-templates/degree-templates.module#DegreeTemplatesModule'
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class DepartmentsRoutingModule {
}
