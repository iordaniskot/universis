import {Component, Input, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';
import {ActiveDepartmentService} from "../../../../../registrar-shared/services/activeDepartmentService.service";

@Component({
  selector: 'app-departments-dashboard-overview-registrations',
  templateUrl: './departments-dashboard-overview-registrations.component.html',
})
export class DepartmentsDashboardOverviewRegistrationsComponent  implements OnInit, OnDestroy {
  public activeDepartment: any;
  public registrationStatus: string;
  public departmentsId: any;
  public department: any;
  public paramSubscription: any;
  constructor(private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext,
              private _activeDepartmentService: ActiveDepartmentService) {}

  async ngOnInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe(async params => {
      this.departmentsId = params.id;
      this.activeDepartment = await this._activeDepartmentService.getActiveDepartment();
    this.department = await this._context.model('LocalDepartments')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .getItem();
      this.registrationStatus = this.getRegistrationStatus();
    if(this.department.id === this.activeDepartment.id){
      if(!this._activeDepartmentService.checkDepartmentsEqual(this.activeDepartment, this.department)){
        this._activeDepartmentService.setActiveDepartment(this.department);
        this.activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      }
    }
    });
    
  }
  getRegistrationStatus(): string {
    const startingDate = new Date(this.department.registrationPeriodStart);
    const endingDate = new Date(this.department.registrationPeriodEnd);
    const now = new Date();

    if (startingDate > now) {
      return 'pendingStart';
    } else if ( this.department.isRegistrationPeriod && (startingDate <= now && endingDate >= now)) {
      return 'open';
    } else {
      return 'close';
    }
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
}
