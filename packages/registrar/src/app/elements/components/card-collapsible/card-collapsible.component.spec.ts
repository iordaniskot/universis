import { TestBed, async } from '@angular/core/testing';
import {Component, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {CardCollapsibleComponent} from './card-collapsible.component';
import {TranslateModule} from '@ngx-translate/core';

// noinspection AngularMissingOrInvalidDeclarationInModule
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'universis-test-root',
    template: `<div>
        <card-collapsible [collapsed]="false">
            <card-header>Collapsible header</card-header>
            <card-body>Collapsible body</card-body>
        </card-collapsible>
    </div>`
})
// ts-ignore
export class TestAppComponent {
    title = 'registrar';
}


describe('CardCollapsibleComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                FormsModule,
                TranslateModule.forRoot()
            ],
            declarations: [
                TestAppComponent,
                CardCollapsibleComponent
            ],
            schemas: [
                CUSTOM_ELEMENTS_SCHEMA
            ]
        }).compileComponents();
    }));

    it('should get collapsible card header', () => {
        const fixture = TestBed.createComponent(TestAppComponent);
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h5').textContent).toContain('Collapsible header');
    });

    it('should get collapsible card body', () => {
        const fixture = TestBed.createComponent(TestAppComponent);
        fixture.detectChanges();
        const compiled = <HTMLDivElement>fixture.debugElement.nativeElement;
        expect(compiled.querySelector('.card-body').textContent).toContain('Collapsible body');
    });

    it('should toggle card body', () => {
        const fixture = TestBed.createComponent(TestAppComponent);
        fixture.detectChanges();
        const compiled = <HTMLDivElement>fixture.debugElement.nativeElement;
        expect(compiled.querySelector('.card-body')).toBeTruthy();
        compiled.querySelector('button').click();
        fixture.detectChanges();
        expect(compiled.querySelector('.card-body')).toBeFalsy();
    });

});
