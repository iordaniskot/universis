import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThesesHomeComponent } from './components/theses-home/theses-home.component';
import {ThesesSharedModule} from './theses.shared';
import {ThesesRoutingModule} from './theses.routing';
import {TablesModule} from '@universis/ngx-tables';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {ThesesTableComponent} from './components/theses-table/theses-table.component';
import { ThesesPreviewComponent } from './components/theses-preview/theses-preview.component';
import { ThesesRootComponent } from './components/theses-root/theses-root.component';
import {SharedModule, LocalizedAttributesPipe} from '@universis/common';
import {FormsModule} from '@angular/forms';
import {ElementsModule} from '../elements/elements.module';
import {StudentsSharedModule} from '../students/students.shared';
import {ThesesAdvancedTableSearchComponent} from './components/theses-table/theses-advanced-table-search.component';
import {MostModule} from '@themost/angular';
import { AdvancedFormsModule } from '@universis/forms';
import {RegistrarSharedModule} from '../registrar-shared/registrar-shared.module';
import {RouterModalModule} from '@universis/common/routing';
import {ThesesAddStudentComponent} from './components/theses-dashboard/theses-dashboard-students/theses-add-student.component';
import {ThesesDashboardComponent} from './components/theses-dashboard/theses-dashboard.component';
import {ThesesDashboardGradesComponent} from './components/theses-dashboard/theses-dashboard-grades/theses-dashboard-grades.component';
// tslint:disable-next-line:max-line-length
import {ThesesDashboardGradesStudentsFormComponent} from './components/theses-dashboard/theses-dashboard-grades/theses-dashboard-grades-students-form.component';
// tslint:disable-next-line:max-line-length
import {ThesesDashboardGradesInstructorsFormComponent} from './components/theses-dashboard/theses-dashboard-grades/theses-dashboard-grades-instructors-form.component';
// tslint:disable-next-line:max-line-length
import {ThesesDashboardStudentsComponent} from './components/theses-dashboard/theses-dashboard-students/theses-dashboard-students.component';
// tslint:disable-next-line:max-line-length import-spacing
import  {ThesesDashboardStudentsFormComponent} from './components/theses-dashboard/theses-dashboard-students/theses-dashboard-students-form.component';
// tslint:disable-next-line:max-line-length import-spacing
import  {ThesesDashboardOverviewComponent} from './components/theses-dashboard/theses-dashboard-overview/theses-dashboard-overview.component';
// tslint:disable-next-line:max-line-length
import {ThesesDashboardOverviewGeneralComponent} from './components/theses-dashboard/theses-dashboard-overview/theses-dashboard-overview-general/theses-dashboard-overview-general.component';
// tslint:disable-next-line:max-line-length
import {ThesesDashboardOverviewStudentsComponent} from './components/theses-dashboard/theses-dashboard-overview/theses-dashboard-overview-students/theses-dashboard-overview-students.component';
// tslint:disable-next-line:max-line-length
import {ThesesDashboardOverviewGradesComponent} from './components/theses-dashboard/theses-dashboard-overview/theses-dashboard-overview-grades/theses-dashboard-overview-grades.component';
import {ThesesDashboardMembersComponent} from './components/theses-dashboard/theses-dashboard-members/theses-dashboard-members.component';
import {ThesesAddMemberComponent} from './components/theses-dashboard/theses-dashboard-members/theses-add-member.component';
// tslint:disable-next-line:max-line-length
import {ThesesDashboardOverviewMembersComponent} from './components/theses-dashboard/theses-dashboard-overview/theses-dashboard-overview-members/theses-dashboard-overview-members.component';
// tslint:disable-next-line:max-line-length
import { ThesesDashboardMembersFactorsComponent } from './components/theses-dashboard/theses-dashboard-members/theses-dashboard-members-factors/theses-dashboard-members-factors.component';

@NgModule({
  imports: [
    CommonModule,
    ThesesSharedModule,
    ThesesRoutingModule,
    TranslateModule,
    TablesModule,
    FormsModule,
    ElementsModule,
    SharedModule,
    StudentsSharedModule,
    MostModule,
    AdvancedFormsModule,
    RegistrarSharedModule,
    RouterModalModule,
  ],
  declarations: [ThesesHomeComponent,
    ThesesTableComponent,
    ThesesPreviewComponent,
    ThesesRootComponent,
    ThesesAdvancedTableSearchComponent,
    ThesesAddStudentComponent,
    ThesesDashboardComponent,
    ThesesDashboardGradesComponent,
    ThesesDashboardGradesStudentsFormComponent,
    ThesesDashboardGradesInstructorsFormComponent,
    ThesesDashboardStudentsComponent,
    ThesesDashboardStudentsFormComponent,
    ThesesDashboardOverviewComponent,
    ThesesDashboardOverviewGeneralComponent,
    ThesesDashboardOverviewStudentsComponent,
    ThesesDashboardOverviewGradesComponent,
    ThesesDashboardMembersComponent,
    ThesesAddMemberComponent,
    ThesesDashboardOverviewMembersComponent,
    ThesesDashboardMembersFactorsComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ThesesModule {

  constructor(private _translateService: TranslateService) {
    //
  }
}
