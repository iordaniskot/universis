import { Component, OnDestroy, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import {
  AppEventService,
  DIALOG_BUTTONS,
  ErrorService,
  GradeScale,
  GradeScaleService,
  LoadingService,
  ModalService
} from '@universis/common';
import { AngularDataContext } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-theses-dashboard-instructors',
  templateUrl: './theses-dashboard-grades.component.html',
})
export class ThesesDashboardGradesComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  public model: any;
  public members: any;
  public studentsGrades: any;
  public editMode = false;
  public editableGrades: boolean;
  public thesisGradeScale: GradeScale;
  public gsIsNumeric: boolean;
  public gradeScaleValues: any[];
  public lastError: any;
  private changeSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _modalService: ModalService,
              private _translateService: TranslateService,
              private _gradeScaleService: GradeScaleService,
              private _loadingService: LoadingService,
              private _appEvent: AppEventService,
              private _errorService: ErrorService) {}

  async ngOnInit() {

    this._loadingService.showLoading();
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      const thesisId = params.id;
      await this.load(thesisId);
      this.changeSubscription = this._appEvent.changed.subscribe(async (event) => {
        if (event && (event.model === 'Theses') && event.target ) {
          // reload
          await this.load(event.target);
        }
      });
      this._loadingService.hideLoading();
    });
  }

  private async load(thesisId) {
    // fetch thesis students
    const model = await this._context.model('Theses')
      .where('id').equal(thesisId)
      .expand('gradeScale,instructor,type,status')
      .getItem();
    const students = await this._context.model(`Theses/${thesisId}/students`)
      .asQueryable()
      .expand('student($expand=department,person,studentStatus)')
      .getItems();
    this.model = Object.assign(model, {
      students
    });
    // get thesis grade scale.
    this.thesisGradeScale = await this._gradeScaleService.getGradeScale(this.model.gradeScale.id);

    // check if gradeScale is numeric.
    if (this.thesisGradeScale.scaleType === 0) {
      this.gsIsNumeric = true;
    } else /* non-numeric gradeScale */ {
      // get categorical gradeScale values -> to be used in dropdown.
      this.gsIsNumeric = false;
      this.gradeScaleValues = [];
      this.thesisGradeScale['values'].forEach(value => {
        this.gradeScaleValues.push(value.alternateName);
      });
    }

    if (this.model.students && this.model.students.length > 0) {
      // set grades to be editable only if the thesis' status is active or potential-new
      // and the student's status is active.

      this.model.students.map( s => {
        s.editableGrades = (this.model.status.alternateName === 'active' || this.model.status.alternateName === 'potential')
          && (s.student.studentStatus.alternateName === 'active');
        return s;
      });

      this.editableGrades = this.model.students.filter( s => {
        return s.editableGrades;
      }).length;

      // fetch student thesis results.
      const studentThesisResults = await this._context.model('StudentThesisResults')
        .where('thesis').equal(thesisId)
        .expand('instructor')
        .orderBy('index')
        .getItems();

      // set thesis results per student.
      this.model.students.forEach(student => {
        student.thesisResults = studentThesisResults.filter(result => {
          return result.student === student.student.id;
        });
      });
    }

  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  getGradeSubmissionConfirmation() {
    // clear error
    this.lastError = null;
    // validate grades.
    this.validateGrades();
    // if there are no errors
    if (this.lastError === null) {
      // get confirmation regarding the grade submission procedure
      return this._modalService.showDialog(
        this._translateService.instant('Theses.UpdateGrades'),
        this._translateService.instant('Theses.UpdateGradesMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
        // if the user confirms the action, proceed.
        if (result === 'ok') {
          this.saveGrades();
        }
      });
    }
  }

  async saveGrades() {
    this._loadingService.showLoading();
    const thesisResultsToBePosted = [];
    // get results to be posted.
    this.model.students.forEach(student => {
      student.thesisResults.map(result => {
        return {
          thesis: this.model.id,
          student: student.student.id,
          studentThesis: student.id,
          instructor: result.instructor.id,
          grade: result.grade
        };
      }).forEach(result => {
        thesisResultsToBePosted.push(result);
      });
    });
    // save results.
    await this._context.model('StudentThesisResults').save(thesisResultsToBePosted);
    this.editMode = false;
    this._loadingService.hideLoading();
    // fetch data.
    this.ngOnDestroy();
    this.ngOnInit();
  }

  convertGrade(grade: any): number {
    try {
      return this.thesisGradeScale.convert(grade);
    } catch (err) {
      // prevent error stacking.
      if (this.lastError === null) {
        this._modalService.showErrorDialog(this._translateService.instant('Theses.UpdateGrades'),
          this._translateService.instant('Theses.GradeError'));
      }
      this.lastError = err;
    }
  }

  toggleEditMode(): void {
    this.editMode = !this.editMode;
  }

  validateGrades(): void {
    this.model.students.forEach(student => {
      student.thesisResults.forEach(result => {
        if (result.formattedGrade === '') {
          // null value is acceptable.
          result.grade = null;
        } else {
          // convert formattedGrade to grade via gradeScaleService.
          result.grade = this.convertGrade(result.formattedGrade);
        }
      });
    });
  }
}
