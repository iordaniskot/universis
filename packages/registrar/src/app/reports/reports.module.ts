import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { ReportsSharedModule } from '../reports-shared/reports-shared.module';
import { SharedModule } from '@universis/common';
import { ReportsRoutingModule } from './reports.routing';
import { ReportTemplateListComponent } from './components/report-template-list/list.component';
import { ReportCategoryListComponent } from './components/report-category-list/list.component';
import { TablesModule } from '@universis/ngx-tables';
import { RouterModalModule } from '@universis/common/routing';
import { MostModule } from '@themost/angular';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    SharedModule,
    TablesModule,
    MostModule,
    TranslateModule,
    RouterModalModule,
    ReportsSharedModule,
    ReportsRoutingModule
  ],
  declarations: [
    ReportTemplateListComponent,
    ReportCategoryListComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ReportsModule {
  constructor() {
  }
}
