import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-classes-overview-general',
  templateUrl: './classes-overview-general.component.html',
  styleUrls: ['./classes-overview-general.component.scss']
})
export class ClassesOverviewGeneralComponent implements OnInit, OnDestroy  {

  public class: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.class = await this._context.model('CourseClasses')
        .where('id').equal(params.id)
        .expand('period, status, course($expand=department),statistic')
        .getItem();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
