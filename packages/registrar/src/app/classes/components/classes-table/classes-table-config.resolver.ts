import {Injectable} from '@angular/core';
import {TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
export class ClassTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./classes-table.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`./classes-table.config.list.json`);
        });
    }
}

export class ClassTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./classes-table.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`./classes-table.search.list.json`);
            });
    }
}

export class ClassDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`./classes-table.config.list.json`);
    }
}
