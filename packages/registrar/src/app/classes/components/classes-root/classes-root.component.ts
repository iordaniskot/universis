import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import * as CLASSES_LIST_CONFIG from '../classes-table/classes-table.config.list.json';
import {cloneDeep} from 'lodash';
import {AppEventService, TemplatePipe} from '@universis/common';
import {Subscription} from 'rxjs';
import {ActiveDepartmentService} from '../../../registrar-shared/services/activeDepartmentService.service';

@Component({
  selector: 'app-classes-root',
  templateUrl: './classes-root.component.html',
})
export class ClassesRootComponent implements OnInit, OnDestroy  {
  public model: any;
  public classId: any;
  public config: any;
  public actions: any[];
  public allowedActions: any[];
  public edit: any;
  private subscription: Subscription;


  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _template: TemplatePipe,
              private _appEvent: AppEventService,
              private _activeDepartmentService: ActiveDepartmentService) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.classId = params.id;

      this.model = await this._context.model('CourseClasses')
        .where('id').equal(params.id)
        .expand('course,status,period')
        .getItem();


      // @ts-ignore
      this.config = cloneDeep(CLASSES_LIST_CONFIG as TableConfiguration);

      if (this.config.columns && this.model) {
        // get actions from config file
        const actions = this.config.columns.filter(x => {
          return x.actions;
        })
          // map actions
          .map(x => x.actions)
          // get list items
          .reduce((a, b) => b, 0);

        // filter actions with student permissions
        const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
        // filter actions by current department
        this.allowedActions = actions.slice().filter(x => {
          if (x.role) {
              if (x.access && x.access.length > 0) {
                let access = x.access;
                access = access.filter(y => {
                  if (y.department && y.department === 'current') {
                    return this.model.course.department === activeDepartment.id;
                  } else {
                    return true;
                  }
                });
                if (access && access.length > 0) {
                  return x;
                }
              } else {
                return x;
              }
            }
        });

        this.edit = this.allowedActions.find(x => {
          if (x.role === 'edit') {
            x.href = this._template.transform(x.href, this.model);
            return x;
          }
        });

        this.actions = this.allowedActions;
        this.actions.forEach(action => {
          action.href = this._template.transform(action.href, this.model);
        });

      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
