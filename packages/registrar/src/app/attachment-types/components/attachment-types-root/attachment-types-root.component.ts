import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import * as AttachmentTypes_LIST_CONFIG from '../attachment-types-table/attachment-types-table.config.list.json';
import {TableConfiguration} from '@universis/ngx-tables';
import {cloneDeep} from 'lodash';
import {TemplatePipe} from '@universis/common';

@Component({
  selector: 'app-attachment-types-root',
  templateUrl: './attachment-types-root.component.html',
  styleUrls: ['./attachment-types-root.component.scss'],
  providers: [TemplatePipe]
})
export class AttachmentTypesRootComponent implements OnInit {
  public model: any;
  public actions: any[];
  public config: any;
  public isCreate = false;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _template: TemplatePipe) { }

  async ngOnInit() {
    if (this._activatedRoute.snapshot.url.length > 0 &&
      this._activatedRoute.snapshot.url[0].path === 'create') {
      this.isCreate = true;
    } else {
      this.isCreate = false;
    }

    if (this._activatedRoute.snapshot.params.id) {
      this.model = await this._context.model('AttachmentTypes')
        .where('id').equal(this._activatedRoute.snapshot.params.id)
        .getItem();

      // @ts-ignore
      this.config = cloneDeep(AttachmentTypes_LIST_CONFIG as TableConfiguration);

      if (this.config.columns) {
        this.actions = this.config.columns.filter(x => {
          return x.actions;
        })
          // map actions
          .map(x => x.actions)
          // get list items
          .reduce((a, b) => b, 0);
        this.actions.forEach(action => {
          action.href = this._template.transform(action.href, this.model);
        })
      }
    }
  }
}
