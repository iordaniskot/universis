import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationsPreviewGeneralComponent } from './registrations-preview-general.component';

describe('RegistrationsPreviewGeneralComponent', () => {
  let component: RegistrationsPreviewGeneralComponent;
  let fixture: ComponentFixture<RegistrationsPreviewGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationsPreviewGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationsPreviewGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
