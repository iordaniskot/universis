import { KeyValue } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, LoadingService, ToastService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { ActiveDepartmentService } from 'packages/registrar/src/app/registrar-shared/services/activeDepartmentService.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-students-registration-new',
  templateUrl: './students-registration-new.component.html',
})
export class StudentsRegistrationNewComponent extends RouterModalOkCancel implements OnInit, OnDestroy {
  private subscription: Subscription;
  public model: any;
  public options: Map<number, Map<string, any>> = new Map();
  public lastError: any = null;
  public noOptions = false;
  public isLoading = true;
  public studentId: number;
  public selectedYear: number;
  public selectedPeriod: any;

  constructor(protected _router: Router,
    protected _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _activeDepartmentService: ActiveDepartmentService,
    private _translateService: TranslateService,
    private _toastService: ToastService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService) {
    // call super constructor.
    super(_router, _activatedRoute);
  }

  private areNull(...args: Array<Array<any>>) {
    for (const arg of args) {
      if (!arg || arg.length === 0) { return true; }
    }
  }

  private setNoOptions() {
    this._loadingService.hideLoading();
    this.isLoading = false;
    this.noOptions = true;
  }

  ngOnInit() {
    this._loadingService.showLoading();
    // set up modal.
    this.modalTitle = this._translateService.instant('Students.Registration.NewRegistration');
    this.okButtonText = this._translateService.instant('OK');
    this.cancelButtonText = this._translateService.instant('Cancel');
    this.modalClass = 'modal-lg';
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      try {
        // get student data.
        this.model = await this._context.model(`Students/${params.id}`)
          .asQueryable()
          .expand('registrations')
          .select('inscriptionYear/id as inscriptionYear, inscriptionPeriod/id as inscriptionPeriod, registrations, id')
          .getItem();

        // get currentYear, currentPeriod.
        const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
        let currentYear: number, currentPeriod: number;
        if (activeDepartment) {
          currentPeriod = activeDepartment.currentPeriod.id,
            currentYear = activeDepartment.currentYear.id;
        }

        // get all academic years between student's inscriptionYear and currentYear
        const years = (this.model && this.model.inscriptionYear && currentYear) ? await this._context.model('AcademicYears')
          .where('id').between(this.model.inscriptionYear, currentYear)
          .take(-1)
          .orderByDescending('id')
          .getItems()
          : null;

        const periods = await this._context.model(`AcademicPeriods`)
          .take(-1)
          .getItems();

        //  if years or periods are null, no options should be available to the user
        if (this.areNull(years, periods)) { return this.setNoOptions(); }

        // the following map is used to easily check if the student has a registration for a specific year and period
        const registrationMap: Map<number, Map<number, boolean>> = new Map();
        for (const registration of this.model.registrations) {
          if (!registrationMap.has(registration.registrationYear)) {
            registrationMap.set(registration.registrationYear, new Map());
          }
          registrationMap.get(registration.registrationYear).set(registration.registrationPeriod, true);
        }

        // create the options that will be displayed in the form
        const inscriptionYear = this.model.inscriptionYear;
        const inscriptionPeriod = this.model.inscriptionPeriod;
        for (const year of years) {
          const optionsKey = year.id;
          for (const period of periods) {
            // don't include the first period if the student was inscribed in the second period
            // and don't include the second period if the current period is the first
            if (inscriptionPeriod > period.id && inscriptionYear === year.id) { continue; }
            if (currentPeriod < period.id && currentYear === year.id) { continue; }

            if (!(registrationMap.has(year.id) && registrationMap.get(year.id).has(period.id))) {
              if (!this.options.has(optionsKey)) {
                if (!this.selectedYear) { this.selectedYear = year.id; } // set default selectedYear
                this.options.set(optionsKey, new Map([['periods', []], ['alternateName', year.alternateName]]));
              }
              this.options.get(optionsKey).get('periods').push({ alternateName: period.alternateName, id: period.id });
            }
          }
        }

        // if this.options has no values, no options should be available to the user
        if (this.areNull(Array.from(this.options.values()))) { return this.setNoOptions(); }

        this.selectPeriod(); // set default selectedPeriod
        this.studentId = parseInt(params.id, 10);
        this._loadingService.hideLoading();
        this.isLoading = false;
        // if we are here, there are options available, so change ok button to submit button
        this.okButtonText = this._translateService.instant('Submit');
      } catch (error) {
        this._loadingService.hideLoading();
        this.isLoading = false;
        this.lastError = error;
      }
    });
  }

  selectPeriod() {
    this.selectedPeriod = this.options.get(this.selectedYear).get('periods')[0];
  }

  /*
    Comparator for the keyvalue pipe.
    Order by key (descending)
  */
  keyDescOrder = (a: KeyValue<number, any>, b: KeyValue<number, any>): number => {
    return a.key > b.key ? -1 : (b.key > a.key ? 1 : 0);
  }

  async ok() {
    let error = null;

    // if there are no options or an error occurred in ngOnInit, just close the modal
    if (this.noOptions || this.lastError) { return this.close(); }

    try {
      const newRegistration = {
        student: this.studentId,
        registrationYear: this.selectedYear,
        registrationPeriod: this.selectedPeriod.id,
        registrationDate: new Date(),
        status: {
          alternateName: 'open'
        }
      };

      const registrationResult = await this._context.model(`Students/${this.studentId}/registrations`).save(newRegistration);
      // catch validation result error
      if (registrationResult && registrationResult.validationResult && registrationResult.validationResult.success === false) {
        throw new Error(registrationResult.validationResult.innerMessage || registrationResult.validationResult.message);
      }
    } catch (err) {
      error = err;
    }
    // close and show error or success message
    return this.close({
      fragment: 'reload',
      skipLocationChange: true
    }).then(() => {
      if (error) {
        this._errorService.showError(error, {
          continueLink: '.'
        });
      } else {
        this._toastService.show(this._translateService.instant('Students.Registration.SuccessTitle'),
                                this._translateService.instant('Students.Registration.SuccessMsg'));
      }
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  cancel() {
    // close.
    return this.close();
  }
}
