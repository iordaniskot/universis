import { Component, Input, OnInit, EventEmitter, Output, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { ConfigurationService } from '@universis/common';
import * as Quill from 'quill'
@Component({
  selector: 'app-compose-message',
  templateUrl: './compose-message.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [
    `
    .ql-toolbar.ql-snow {
      border: 1px solid #E4E7EA;
    }
    .ql-toolbar {
      border-top-left-radius: .375rem !important;
      border-top-right-radius: .375rem !important;
    }
    .ql-container.ql-snow {
      border: 1px solid #E4E7EA;
    }
    .ql-container {
      font-family: inherit;
      line-height: 1.75;
      min-height: 192px;
      font-size: .875rem;
      border-bottom-left-radius: .375rem !important;
      border-bottom-right-radius: .375rem !important;
    }
    `
  ]
})
export class ComposeMessageComponent implements OnInit {

  @Input() model: { subject: string; body: string; attachments: any[] };
  @Output() modelChange: EventEmitter<any> = new EventEmitter();
  public currentLang: any;
  @Output() submit: EventEmitter<any> = new EventEmitter();
  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Input() showAttach = true;
  @Input() showButtons = true;
  @ViewChild('messageBody') messageBody: ElementRef;
  constructor(private _configurationService: ConfigurationService) { }

  ngOnInit() {
    this.currentLang = this._configurationService.currentLocale;
    const QuillEditor: any = Quill;
    const messageBodyEditor = new QuillEditor(this.messageBody.nativeElement, {
      theme: 'snow'
    });
    messageBodyEditor.on('text-change', (event) => {
      this.model.body = messageBodyEditor.getSemanticHTML();
    });

  }

  onFileAdd(event: any) {
    const addedFile = event.addedFiles[0];
    this.model.attachments = [
      addedFile
    ];
  }

  onFileRemove(event: any) {
    this.model.attachments = [];
  }



}
