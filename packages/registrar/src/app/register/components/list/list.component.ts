import { Component, OnInit, ViewChild, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { AdvancedTableComponent, AdvancedTableDataResult } from '@universis/ngx-tables';
import { AdvancedTableSearchComponent } from '@universis/ngx-tables';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import { Subscription, Observable } from 'rxjs';
import * as StudyProgramRegisterActionTable from './StudyProgramRegisterActionTable.json';
import * as StudyProgramRegisterActionSearch from './StudyProgramRegisterActionSearch.json';
import { AngularDataContext } from '@themost/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { UserActivityService, AppEventService, LoadingService, ModalService, ErrorService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedTableService } from '@universis/ngx-tables';
import { ClientDataQueryable } from '@themost/client';
import { RequestActionComponent } from '../../../requests/components/request-action/request-action.component';
import { SendMessageActionComponent } from '../send-message-action/send-message-action.component';
import * as moment from 'moment';

@Component({
  selector: 'app-candidate-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit, OnDestroy {

  public recordsTotal: any;
  private dataSubscription: Subscription;
  private changeSubscription: Subscription;
  @Input() tableConfiguration: any = StudyProgramRegisterActionTable;
  @Input() searchConfiguration: any = StudyProgramRegisterActionSearch;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  selectedItems: any[];

  constructor(private _context: AngularDataContext,
    private _router: Router,
    private _activatedTable: ActivatedTableService,
    private _activatedRoute: ActivatedRoute,
    private _userActivityService: UserActivityService,
    private _translateService: TranslateService,
    private _appEvent: AppEventService,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService) { }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {

      this._activatedTable.activeTable = this.table;
      // set search form
      this.search.form = this.searchConfiguration;
      Object.assign(this.search.form, { department: this._activatedRoute.snapshot.data.department.id });
      this.search.ngOnInit();
      // set table config and recall data
      if (this.tableConfiguration) {
        // set config
        this.table.config = this.tableConfiguration;
        // reset search text
        this.advancedSearch.text = null;
        // reset table
        this.table.reset(false);
      }

      this._userActivityService.setItem({
        category: this._translateService.instant('Classes.Candidates.Many'),
        description: this._translateService.instant('List'),
        url: window.location.hash.substring(1), // get the path after the hash
        dateCreated: new Date
      });
    });

    this.changeSubscription = this._appEvent.changed.subscribe((event) => {
      if (this.table.dataTable == null) {
        return;
      }
      if (event && event.target && event.model === 'StudyProgramRegisterActions') {
        this.table.fetchOne({
          result: event.target.id
        });
      }
      if (event && event.target && event.model === this.table.config.model) {
        this.table.fetchOne({
          id: event.target.id
        });
      }
    });

  }

  accept() {
    //
  }

  reject() {

  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'actionStatus/alternateName as actionStatus',
          'owner', 'candidate/id as candidateIdentifier', 'candidate/person/email as email'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.table.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map((item) => {
            return {
              id: item.id,
              actionStatus: item.actionStatus,
              owner: item.owner,
              candidateIdentifier: item.candidateIdentifier,
              email: item.email
            };
          });
        }
      }
    }
    return items;
  }

  executeChangeActionStatus(statusText: string) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // set active item
            const updated = {
              id: item.id,
              actionStatus: {
                alternateName: statusText
              }
            };
            await this._context.model(this.table.config.model).save(updated);
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: updated.id
              });
            } catch (err) {
              //
            }

          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }

        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }


  executeSendMessageAction(message) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const newMessage = Object.assign({}, message, {
              recipient: item.owner
            });
            await await this._context.model(`StudyProgramRegisterActions/${item.id}/messages`).save(newMessage);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }

        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  /**
   * Rejects the selected requests
   */
  async rejectAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active items
      this.selectedItems = items.filter((item) => {
        return item.actionStatus === 'ActiveActionStatus' || item.actionStatus === 'PotentialActionStatus';
      });
      // check if enrollment period has expired for requests with potential status
      for (let i = 0; i < this.selectedItems.length; i++) {
        if (this.selectedItems[i].actionStatus === 'PotentialActionStatus') {
          this.selectedItems[i].expired = false;
          const request = await this._context.model('StudyProgramRegisterActions').where('id')
            .equal(this.selectedItems[i].id)
            .expand('studyProgramEnrollmentEvent')
            .getItem();
          // check enrollement period
          this.selectedItems[i].expired = !this.validateEnrollmentPeriod(request.studyProgramEnrollmentEvent);
        }
      }
      this.selectedItems = this.selectedItems.filter(x => {
        return x.actionStatus === 'ActiveActionStatus' ||
          (x.actionStatus === 'PotentialActionStatus' && x.expired === true);
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Register.RejectAction.Title',
          description: 'Register.RejectAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeActionStatus('CancelledActionStatus')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  validateEnrollmentPeriod(enrollmentPeriod) {
    const now = moment(new Date()).startOf('day').toDate();
    let valid = false;
    if (enrollmentPeriod.validFrom instanceof Date) {
      if (enrollmentPeriod.validThrough instanceof Date) {
        valid = enrollmentPeriod.validFrom <= now && enrollmentPeriod.validThrough >= now;
      } else {
        valid = enrollmentPeriod.validFrom <= now;
      }
    } else if (enrollmentPeriod.validThrough instanceof Date) {
      valid = enrollmentPeriod.validThrough >= now;
    }
    return valid;
  }

  async sendMessageAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items;
      this._loadingService.hideLoading();
      const message = {};
      this._modalService.openModalComponent(SendMessageActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Register.ComposeNewMessage.Title',
          description: 'Register.ComposeNewMessage.Description',
          refresh: this.refreshAction,
          message: message,
          execute: this.executeSendMessageAction(message)
        }
      });
    } catch (err) {
      console.log(err);
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeSendDirectMessageAction(message) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const newMessage = Object.assign({}, message, {
              recipient: item.owner,
              candidateStudent: item.candidateIdentifier
            });
            await this._context.model(`CandidateStudents/${item.candidateIdentifier}/sendMessage`).save(newMessage);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }

        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async sendDirectMessageAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // regex source: https://github.com/formio/formio.js/blob/master/src/validator/rules/Email.js#L13
      // tslint:disable-next-line: max-line-length
      const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      this.selectedItems = items.filter(candidate => re.test(candidate.email));
      this._loadingService.hideLoading();
      const message = {};
      this._modalService.openModalComponent(SendMessageActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Register.SendDirectMessageToCandidate.Title',
          description: 'Register.SendDirectMessageToCandidate.Description',
          refresh: this.refreshAction,
          message: message,
          execute: this.executeSendDirectMessageAction(message)
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async acceptAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active items
      this.selectedItems = items.filter((item) => {
        return item.actionStatus === 'ActiveActionStatus';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Requests.Edit.AcceptAction.Title',
          description: 'Requests.Edit.AcceptAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeActionStatus('CompletedActionStatus')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

}
