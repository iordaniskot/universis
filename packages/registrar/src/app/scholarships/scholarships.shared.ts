import {NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateService, TranslateModule} from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@universis/common';
import { ScholarshipsPreviewGeneralInfoFormComponent
} from './components/scholarships-preview/scholarships-preview-general/scholarships-preview-general-info-form.component';
import {
  ScholarshipsDefaultTableConfigurationResolver, ScholarshipsTableConfigurationResolver,
  ScholarshipsTableSearchResolver
} from './components/scholarships-table/scholarships-table-config.resolver';
import {
  ScholarshipResultsDefaultTableConfigurationResolver, ScholarshipResultsTableConfigurationResolver,
  ScholarshipResultsTableSearchResolver
} from './components/scholarships-preview/scholarship-preview-results/scholarship-preview-results-config.resolver';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    TranslateModule
  ],
  declarations: [
    ScholarshipsPreviewGeneralInfoFormComponent
  ],
  providers: [
    ScholarshipsDefaultTableConfigurationResolver,
    ScholarshipsTableConfigurationResolver,
    ScholarshipsTableSearchResolver,
    ScholarshipResultsDefaultTableConfigurationResolver,
    ScholarshipResultsTableSearchResolver,
    ScholarshipResultsTableConfigurationResolver,
  ],
  exports : [
    ScholarshipsPreviewGeneralInfoFormComponent
  ]
})
export class ScholarshipsSharedModule implements OnInit {

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading scholarships shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/scholarships.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }

}
