import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Event, NavigationEnd, Router } from '@angular/router';
import { ErrorService, LoadingService } from '@universis/common';
import { Subscription } from 'rxjs';
import { StatisticsService } from '../../services/statistics-service/statistics.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit, OnDestroy {

  public availableRoutes: any[];
  public _routerEventsSubscription: Subscription;

  constructor(
    private readonly _statisticsService: StatisticsService,
    private readonly _loadingService: LoadingService,
    private readonly _errorService: ErrorService,
    private readonly _router: Router
  ) { }

  async ngOnInit() {
    await this.fetchData();
    this._routerEventsSubscription = this._router.events.subscribe(event => {
      this.handleNavigationToRoot(event);
    });
  }

  async ngOnDestroy() {
    if (this._routerEventsSubscription && !this._routerEventsSubscription.closed) {
      this._routerEventsSubscription.unsubscribe();
    }
  }

  handleNavigationToRoot(routerEvent: Event): void {
    if (
      routerEvent instanceof NavigationEnd 
      && routerEvent.url == '/statistics'
      && this.availableRoutes
      && this.availableRoutes.length > 0
    ) {
      this._router.navigate(['statistics', this.availableRoutes[0].route]);
    }
  }

  async fetchData() {
    this._loadingService.showLoading();
    try {
      this.availableRoutes = (await this._statisticsService.getStatisticsReportCategories())
        .map(category => {
          return {
            route: category.alternateName,
            label: category.name
          };
        });

      if (this.availableRoutes && this.availableRoutes.length > 0) {
        this._router.navigate(['statistics', this.availableRoutes[0].route]);
      } else {
        throw new Error("Error while getting the statistics report categories");
      }
    } catch (err) {
      console.error(err);
      this._errorService.showError(err);
    } finally {
      this._loadingService.hideLoading();
    }
  }
}
