import { Component, OnInit, OnDestroy, Optional, Input, OnChanges, SimpleChanges } from '@angular/core';
import { SettingsService, SettingsSection } from '../../../settings-shared/services/settings.service';
import { Subscription } from 'rxjs';
import { DiagnosticsService } from '@universis/common';
import * as fromSettings from '../../reducers';
import { select, Store } from '@ngrx/store';
import { first } from 'rxjs/operators';
import { AppendFilter } from '../../actions';

@Component({
  selector: 'app-settings-sections',
  templateUrl: './sections.component.html',
  styles: [
    `
    .section-disabled {
      opacity: .5;
    }
    `
  ]
})
export class SectionsComponent implements OnInit, OnDestroy {

  public sections: SettingsSection[];
  @Input() search = '';
  subscription: Subscription;

  constructor(private _settings: SettingsService,
    private diagnostics: DiagnosticsService,
    @Optional() protected store: Store<fromSettings.SettingsState>) { }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onSearchChange(value) {
    if (this.store) {
      this.store.dispatch(new AppendFilter(value));
    }
  }

  clearSearch() {
    this.search = '';
    this.onSearchChange(this.search);
  }

  async tryGetServices() {
    try {
      const services = await this.diagnostics.getServices();
      return services;
    } catch (err) {
      console.error(err);
      return [];
    }
  }

  ngOnInit() {

    if (this.store) {
      this.store.pipe(select(fromSettings.selectFilterText), first()).subscribe((filter) => {
        this.search = filter;
      });
    }
    
    this.tryGetServices().then((services) => {
      this.subscription = this._settings.sections.subscribe( sections => {
        this.sections = sections.sort((a, b) => {
          if (a.description > b.description) {
            return 1;
          }
          if (a.description < b.description) {
            return -1;
          }
          return 0;
        });
          this.sections.forEach((section) => {
            if (section.dependencies && section.dependencies.length) {
                for (const dependency of section.dependencies) {
                  const hasService = services.findIndex((service) => {
                    return service.serviceType === dependency;
                  });
                  if (hasService < 0) {
                    section.hidden = true;
                  }
                }
            }
          });
      });
    });
  }

}
