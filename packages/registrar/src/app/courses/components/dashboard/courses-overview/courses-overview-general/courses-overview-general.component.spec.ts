import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesOverviewGeneralComponent } from './courses-overview-general.component';

describe('CoursesOverviewGeneralComponent', () => {
  let component: CoursesOverviewGeneralComponent;
  let fixture: ComponentFixture<CoursesOverviewGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesOverviewGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesOverviewGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
