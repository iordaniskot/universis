import {AfterViewInit, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import { ActivatedTableService } from '@universis/ngx-tables';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import { Subscription } from 'rxjs';
import * as INSTRUCTORS_THESES_LIST_CONFIG from './instructors-dashboard-theses.config.list.json';
import {LoadingService} from '@universis/common';
declare var $;


@Component({
  selector: 'app-instructors-dashboard-theses',
  templateUrl: './instructors-dashboard-theses.component.html',
  styleUrls: ['../instructors-dashboard.component.scss']
})
export class InstructorsDashboardThesesComponent implements OnInit, OnDestroy {
  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>INSTRUCTORS_THESES_LIST_CONFIG;
  public recordsTotal: any;
  public model2: any;
  private dataSubscription: Subscription;
  @ViewChild('model') model: AdvancedTableComponent;

  @Input() tableConfiguration: any;
  private subscription: Subscription;

  constructor(private _element: ElementRef,
              private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _activatedTable: ActivatedTableService,
              private _loadingService: LoadingService,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this._activatedTable.activeTable = this.model;

    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model.query = this._context.model('StudentTheses')
        .where('thesis/instructor').equal(params.id)
        .or('thesis/members/member').equal(params.id)
        .expand('thesis($expand=startYear,instructor,status,type)')
        .prepare();
      this._loadingService.hideLoading();

      this.model.config = AdvancedTableConfiguration.cast(INSTRUCTORS_THESES_LIST_CONFIG);
      this.model.fetch();
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}
