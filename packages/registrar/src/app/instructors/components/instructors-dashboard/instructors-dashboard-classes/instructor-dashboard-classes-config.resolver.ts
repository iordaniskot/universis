import {TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class InstructorDashboardClassesConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./instructors-dashboard-classes.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`./instructors-dashboard-classes.config.list.json`);
        });
    }
}

export class InstructorDashboardClassesSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./instructors-dashboard-classes.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`./instructors-dashboard-classes.search.list.json`);
            });
    }
}

export class InstructorDefaultDashboardClassesConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`./instructors-dashboard-classes.config.list.json`);
    }
}